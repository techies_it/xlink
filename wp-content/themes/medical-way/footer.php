<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Medical_Way
 */

	/**
	 * Hook - medical_way_after_content.
	 *
	 * @hooked medical_way_after_content_action - 10
	 */
	do_action( 'medical_way_after_content' );

?>

	<?php get_template_part( 'template-parts/footer-widgets' ); ?>

	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="container">
			<?php $copyright_text = medical_way_get_option( 'copyright_text' ); ?>
			<?php if ( ! empty( $copyright_text ) ) : ?>
				<div class="copyright">
					<?php echo wp_kses_data( $copyright_text ); ?>
				</div><!-- .copyright -->
			<?php endif; ?>

			<?php do_action( 'medical_way_credit' ); ?>
			
		</div><!-- .container -->
	</footer><!-- #colophon -->
</div><!-- #page -->
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script>
	jQuery(document).ready(function(){
		jQuery('[data-toggle="tooltip"]').tooltip();   
	});

	jQuery(".short-text-more").toggle(function(){
		jQuery(this).text(" Read Less..").siblings(".short-text-complete").show();    
	}, function(){
		jQuery(this).text(" Read More..").siblings(".short-text-complete").hide();    
	});

</script>
<style>
.red {
    color: gray !important;
}
	.highlight{
		color:#2fa3ad !important;
	}
</style>
<script>
	jQuery(document).ready(function(){
      jQuery(".use-cases-left h2").click(function(){
		 jQuery(".use-cases-left h2").removeClass("red");
		  jQuery(".use-cases-left h2").removeClass("highlight");
        jQuery(this).addClass("red");
        jQuery(this).addClass("highlight");
		  
      });
		
   });
	
	
	
	
	jQuery(function(){
        var href = location.href;  
        var split = href.split("#");
        var afterSplit = "Error parsing url";
        if(split[1] != null){
            afterSplit = split[1];
        }
		if(afterSplit == 'lb-case-use-button'){
       jQuery('#lb-case-use-button').trigger('click');
	 }
		if(afterSplit == 'intergrated'){ 
		jQuery('#intergrated').trigger('click');
		}
	   if(afterSplit == 'secure-connectivity'){ 
		jQuery('#secure-connectivity').trigger('click');
		}
		if(afterSplit == 'digital-enavlement'){ 
		jQuery('#digital-enavlement').trigger('click');
		}
		
		
    });
var windowsize = jQuery(window).width();
jQuery(document).on('click','#lb-pos-payment-button',function(){
	if(windowsize <= '450'){
		jQuery('html, body').animate({
		    scrollTop: (jQuery('.lb-pos-payment-section').offset().top+= -250)
		},1000);
	} 
});
jQuery(document).on('click','#lb-integrated-payments-button',function(){
	if(windowsize <= '450'){
		jQuery('html, body').animate({
		    scrollTop: (jQuery('.lb-integrated-payments-section').offset().top+= -400)
		},1000);
	} 
});
jQuery(document).on('click','#lb-secure-connectivity-button',function(){
	if(windowsize <= '450'){
		jQuery('html, body').animate({
		    scrollTop: (jQuery('.lb-secure-connectivity-section').offset().top+= -400)
		},1000);
	} 
});
jQuery(document).on('click','#sme-pos-payment-button',function(){
	if(windowsize <= '450'){
		jQuery('html, body').animate({
		    scrollTop: (jQuery('.sme-pos-payment-section').offset().top+= -350)
		},1000);
	} 
});
jQuery(document).on('click','#sme-connectivity-button',function(){
	if(windowsize <= '450'){
		jQuery('html, body').animate({
		    scrollTop: (jQuery('.sme-connectivity-section').offset().top+= -350)
		},1000);
	} 
});
	

jQuery(document).ready(function(){
	jQuery('#search-toggle').click(function() {
		jQuery('.teafields-site-search').toggle();
		return false;
	});
});

	jQuery(document).ready(function() {
   jQuery('div#top-bar').show();
});
	
	jQuery(document).on('click','#sme-pos-payment-button',function(){
	if(windowsize >= '850'){
		jQuery('html, body').animate({
		    scrollTop: (jQuery('.sme-pos-payment-section').offset().top+= -250)
		},1000);
	} 
});	
	
	jQuery(document).on('click','#sme-connectivity-button',function(){
	if(windowsize >= '850'){
		jQuery('html, body').animate({
		    scrollTop: (jQuery('.sme-connectivity-section').offset().top+= -250)
		},1000);
	} 
});	
	
	jQuery(document).on('click','#sme-enablement-button',function(){
	if(windowsize >= '850'){
		jQuery('html, body').animate({
		    scrollTop: (jQuery('.sme-enablement-section').offset().top+= -250)
		},1000);
	} 
});	
		jQuery(document).on('click','#lb-integrated-payments-button',function(){
	if(windowsize >= '850'){
		jQuery('html, body').animate({
		    scrollTop: (jQuery('.lb-integrated-payments-section').offset().top+= -250)
		},1000);
	} 
});
		jQuery(document).on('click','#lb-secure-connectivity-button',function(){
	if(windowsize >= '850'){
		jQuery('html, body').animate({
		    scrollTop: (jQuery('.lb-secure-connectivity-section').offset().top+= -250)
		},1000);
	} 
});
	jQuery(document).on('click','#lb-digital-enablement-button',function(){
	if(windowsize >= '850'){
		jQuery('html, body').animate({
		    scrollTop: (jQuery('.lb-digital-enablement-section').offset().top+= -250)
		},1000);
	} 
});
	
		jQuery(document).on('click','#lb-digital-enablement-button',function(){
	if(windowsize >= '850'){
		jQuery('html, body').animate({
		    scrollTop: (jQuery('.lb-digital-enablement-section').offset().top+= -250)
		},1000);
	} 
});
			jQuery(document).on('click','#lb-pos-payment-button',function(){
	if(windowsize >= '850'){
		jQuery('html, body').animate({
		    scrollTop: (jQuery('.lb-pos-payment-section').offset().top+= -250)
		},1000);
	} 
});
		jQuery(document).on('click','#lb-integrated-payments-button',function(){
	if(windowsize >= '850'){
		jQuery('html, body').animate({
		    scrollTop: (jQuery('.lb-integrated-payments-section').offset().top+= -250)
		},1000);
	} 
});
	jQuery(document).on('click','#lb-secure-connectivity-button',function(){
	if(windowsize >= '850'){
		jQuery('html, body').animate({
		    scrollTop: (jQuery('.lb-secure-connectivity-section').offset().top+= -250)
		},1000);
	} 
});
		jQuery(document).on('click','#lb-digital-enablement-button',function(){
	if(windowsize >= '850'){
		jQuery('html, body').animate({
		    scrollTop: (jQuery('.lb-digital-enablement-section').offset().top+= -250)
		},1000);
	} 
});
	
	
</script>
<?php wp_footer(); ?>
</body>
</html>
