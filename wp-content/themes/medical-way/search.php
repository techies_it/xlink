<?php
/**
 * The template for displaying search results pages.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package Medical_Way
 */

get_header(); ?>

	<section id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
		<div class="searc-result-total">			
<?php
	$allsearch = &new WP_Query("s=$s&showposts=-1");
$key = wp_specialchars($s, 1);
$count = $allsearch->post_count; _e('');
_e('<span class="search-terms">"');  echo $key; _e('"</span>'); _e(' &mdash; ');  echo $count . ' '; _e('results'); 

// Don't forget to reset query
wp_reset_query();
			?>
			</div>	
<?php
		if ( have_posts() ) : 
			/* Start the Loop */
			while ( have_posts() ) : the_post();

				get_template_part( 'template-parts/content' );

			endwhile;

			the_posts_pagination();

		else :

			get_template_part( 'template-parts/content', 'none' );

		endif; ?>

		</main><!-- #main -->
	</section><!-- #primary -->

<?php
do_action( 'medical_way_action_sidebar' );
get_footer();
