��    2      �  C   <      H     I     O     c  
   h     s     {     �     �     �     �     �     �     �     �     �     �     �     �     �                         2     9     ?     N     [     a     j     q     z     �     �  	   �     �  
   �     �     �     �     �     �     �     �                    -     1  �  4  
   �     �     	     	     	     $	     +	     4	     :	     H	     f	     x	     �	     �	     �	     �	     �	     �	     �	     �	     �	     
     	
     '
     /
     4
     G
     c
     l
     t
     }
     �
     �
     �
  
   �
     �
     �
     �
     �
     �
               $     +     G     T     c     t     x     (   )                    ,         	          2   1          #      /      -   
   "   *   +       &   '             $       %                                                         0                       !   .                         Agree Back to All Entries Blue Checkboxes Classic Close Columns Date Disagree Download Export (CSV) Dropdown Edit This Form Email Export Green Likert Scale Modern Multiple Choice Name Net Promoter Score Neutral No Open form selector Orange Other Paragraph Text Preview Form Print Purchase Purple Question Rating Red Rows Satisfied Select Form Sending... Service Single Line Text Strongly Agree Strongly Disagree Style Submit Survey Results Unsatisfied Very Satisfied Very Unsatisfied Yes or Project-Id-Version: WPForms Surveys and Polls
Last-Translator: Luis Rull <luisrull@gmail.com>, 2019
Language-Team: Spanish (Spain) (https://www.transifex.com/wp-translations/teams/64435/es_ES/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: es_ES
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-Basepath: ..
X-Poedit-KeywordsList: __;_e;_ex:1,2c;_n:1,2;_n_noop:1,2;_nx:1,2,4c;_nx_noop:1,2,3c;_x:1,2c;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c
X-Poedit-SourceCharset: UTF-8
POT-Creation-Date: 
PO-Revision-Date: 
X-Generator: Poedit 2.2.1
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: *.js
 De acuerdo Volver a todas las entradas Azul Casillas Clásico Cerrar Columnas Fecha En desacuerdo Descargar exportaciones (CSV) Lista desplegable Editar este formulario Correo electrónico Exportar Verde Escala Likert Moderno Opciones múltiple Nombre Puntuación de Net Promoter Neutral No Abrir selector de formularios Naranja Otro Texto del párrafo Vista previa del formulario Imprimir Comprar Púrpura Pregunta Valoración Rojo Filas Satisfecho Elegir formulario Enviando... Servicio Texto de una sola línea Completamente de acuerdo Completamente en desacuerdo Estilo Enviar Resultados de las encuestas Insatisfecho Muy satisfecho Muy insatisfecho Sí o 